#include "JumpPointSearch.h"
#include "IndexedPriorityQueueMin.h"
#include "VMath.h"

namespace via
{

JumpPointSearch::JumpPointSearch(TileWorldRepresentation& t)
	:AStar(t.getGraph()),grid(t){
}


JumpPointSearch::~JumpPointSearch(void)
{
}

std::list<int> JumpPointSearch::search(const SearchHeuristic& searchHeuristic, int startingNodeIndex, int goalNodeIndex)
{
	this->goalNodeIndex = goalNodeIndex;
	this->startingNodeIndex = startingNodeIndex;

	IndexedPriorityQueueMin<float> nodesQueue(expectedCosts,graph.size());
	nodesQueue.insert(startingNodeIndex);

	while(!nodesQueue.empty())
	{
		int currentNodeIndex = nodesQueue.pop();
		const GraphNode& currentNode = graph.getNode(currentNodeIndex);
		shortestPathTree[currentNodeIndex] = frontier[currentNodeIndex];

		if (currentNodeIndex == goalNodeIndex)
		{
			return getPath();
		}

		const std::vector<const Edge>& currentEdges = currentNode.getConnections();

		for (std::vector<const Edge>::const_iterator it = currentEdges.begin(); it != currentEdges.end(); it++)
		{
			const Edge* currentEdge = &(*it);

			const GraphNode& currentNeighbour = graph.getNode((*it).to);

			// Direction from current node to neighbor:
			int currentX,currentY,neighbourX,neighbourY;
			grid.getTileCoordinateFromPosition(currentNode.getPosition(),currentX,currentY);
			grid.getTileCoordinateFromPosition(currentNeighbour.getPosition(),neighbourX,neighbourY);

			int dx = neighbourX - currentX;
			int dy = neighbourY - currentY;

			const GraphNode* jumpingNode = jumpToNode(neighbourX,neighbourY,dx,dy,startingNodeIndex,goalNodeIndex);

			if (jumpingNode)
			{
				/*
				int jumpingNodeIndex = jumpingNode->getIndex();
				float heuristicCost = 	searchHeuristic.calculate(graph, currentNodeIndex,jumpingNodeIndex);
				float realCost = heuristicCost;

				if (frontier[jumpingNodeIndex] == NULL)
				{
					expectedCosts[currentEdge->to] = heuristicCost + realCost;
					cumulativeCosts[currentEdge->to] = realCost;

					nodesQueue.insert(currentEdge->to);
					frontier[currentEdge->to] = &(*currentEdge);
				}else
				{
					if ( realCost < cumulativeCosts[currentEdge->to] && shortestPathTree[currentEdge->to] == NULL)
					{
						expectedCosts[currentEdge->to] = realCost + heuristicCost;
						cumulativeCosts[currentEdge->to] = realCost;

						nodesQueue.updatePriority(currentEdge->to);

						frontier[currentEdge->to] = &(*currentEdge);
					}
				}
				*/
			}
			
		}
	}
	return getPath();
}

const GraphNode* JumpPointSearch::jumpToNode(int currentX, int currentY, int dx, int dy, int startNodeIndex, int goalNodeIndex)
{
	int parentX = currentX + dx;
	int parentY = currentY + dy;

	// return null if the tile is not walkable
	if (!grid.isWalkable(currentX,currentY))
	{
		return 0;
	}

	// if it is the goal node, return it
	if (grid.getNodeFromTile(currentX,currentY).getIndex() == goalNodeIndex)
	{
		return &grid.getNodeFromTile(currentX,currentY);
	}

	// if diagonal, the node is a jump node if its left/right neighbours are forced
	if (dx != 0 && dy != 0)
	{
		if ((grid.isWalkable(currentX-dx,currentY+dy) && !grid.isWalkable(currentX-dx,currentY+dx)) ||
			(grid.isWalkable(currentX+dx,currentY-dy) && !grid.isWalkable(currentX,currentY-dy)))
		{
			return &grid.getNodeFromTile(currentX,currentY);
		}
	}else
	// search along X, the node is a jump node if its top/bottom neighbours are forced
	if (dx != 0)
	{
		
		{
			if ((grid.isWalkable(currentX+dx,currentY+1) && !grid.isWalkable(currentX,currentY+1)) ||
				(grid.isWalkable(currentX+dx,currentY-1) && !grid.isWalkable(currentX,currentY-1)))
			{
				return &grid.getNodeFromTile(currentX,currentY);
			}
		}
	}else
	// search along Y, the node is a jump node if its left/right neighbours are forced
	{
		if ((grid.isWalkable(currentX+1,currentY+dy) && !grid.isWalkable(currentX+1,currentY)) ||
				(grid.isWalkable(currentX-1,currentY-dy) && !grid.isWalkable(currentX-1,currentY)))
			{
				return &grid.getNodeFromTile(currentX,currentY);
			}
	}

	// recursive cases

	// horizontal/verical 
	if ( dx != 0 && dy != 0)
	{
		if (jumpToNode(currentX+dx,currentY,dx,0,startingNodeIndex,goalNodeIndex) != 0)
		{
			return &grid.getNodeFromTile(currentX,currentY);
		}
		if (jumpToNode(currentX,currentY+dy,0,dy,startingNodeIndex,goalNodeIndex) != 0)
		{
			return &grid.getNodeFromTile(currentX,currentY);
		}
	}

	// diagonal
	if (grid.isWalkable(currentX+dx,currentY) || grid.isWalkable(currentX,currentY+dy))
	{
		return jumpToNode(currentX+dx,currentY+dy,dx,dy,startingNodeIndex,goalNodeIndex);
	}

	
	return 0;
}

}
