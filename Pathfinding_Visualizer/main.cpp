#include <SFML\Graphics.hpp>
#include "GraphNode.h"
#include "AStar.h"
#include <iostream>
#include "ViaPathfinding.h"
#include <Windows.h>

#include "world\TileWorldRepresentation.h"

int main()
{

	/*
	via::TileWorldRepresentation tiles (10,10,5.f,true);
	tiles.setTile(0,0,via::TileType::OBSTACLE);
	tiles.setTile(3,0,via::TileType::OBSTACLE);

	via::Graph& g = tiles.createGraph();

	int scale = 10;
	int tileSize = tiles.getTileSize();*/
	
	via::ViaPathfinding pf;

	via::TileWorldRepresentation& tiles = pf.createTileWorldRepresentation(10,10,5.f,true,true);
	tiles.setTile(0,0,via::OBSTACLE);
	tiles.setTile(2,3,via::OBSTACLE);
	tiles.setTile(3,3,via::OBSTACLE);
	tiles.setTile(4,4,via::OBSTACLE);
	tiles.setTile(1,1,via::OBSTACLE);
	tiles.setTile(5,4,via::OBSTACLE);
	tiles.setTile(3,1,via::OBSTACLE);
	tiles.setTile(6,7,via::OBSTACLE);
	tiles.setTile(9,3,via::OBSTACLE);
	tiles.setTile(5,3,via::OBSTACLE);
	tiles.setTile(1,2,via::OBSTACLE);

	int scale = 10;
	float tileSize = tiles.getTileSize();
	const via::Graph& g = pf.createGraphFromCurrentRepresentation();

	sf::RenderWindow window(sf::VideoMode(tileSize * scale * tiles.getWidth() , tileSize * scale * tiles.getHeight() ), "Pathfinding Visualizer",sf::Style::Titlebar | sf::Style::Close);

    sf::Vertex line[] =
{
	sf::Vertex(sf::Vector2f(0, 10),sf::Color(0,0,0)),
	sf::Vertex(sf::Vector2f(window.getSize().x, 10),sf::Color(0,0,0))
};
	int startTileX;
	int startTileY;
	int finalTileX;
	int finalTileY;

	std::list<int> path;
	std::vector<const via::Edge*> pathTree;
	via::GraphNode* node = 0;
	via::GraphNode* goalNode = 0;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();

			if (event.type == sf::Event::MouseButtonPressed)
			{
				sf::Vector2i mousePos = sf::Mouse::getPosition(window);

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					startTileX = (int) (mousePos.x / (tiles.getTileSize() * scale));
					startTileY = (int) (mousePos.y / (tiles.getTileSize() * scale));
				}else
				{
					if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
					{

						finalTileX = (int) (mousePos.x / (tiles.getTileSize() * scale));
						finalTileY = (int) (mousePos.y / (tiles.getTileSize() * scale));
					}
				}
			}

			if (event.type == sf::Event::KeyPressed)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
				{
					node = &tiles.getNodeFromTile(startTileY,startTileX);
					
					goalNode = &tiles.getNodeFromTile(finalTileY,finalTileX);
					SYSTEMTIME st;
					SYSTEMTIME st1;
					GetSystemTime(&st);
					path = pf.findPath(*node,*goalNode);
					GetSystemTime(&st1);
					printf( "%d\n", (st1.wMilliseconds - st.wMilliseconds));
					//path = astar.getPath();
				}
			}
			
        }

		window.clear(sf::Color(255,255,255));
		bool graphEnabled = true;
		int scale = 10;
		
		sf::RectangleShape rect (sf::Vector2f(tiles.getTileSize()*scale,tiles.getTileSize()*scale));
		for (int i = 0; i < tiles.getHeight(); i++)
		{
			for (int j = 0; j < tiles.getWidth(); j++)
			{
				
				rect.setOutlineColor(sf::Color(0,0,0));
				rect.setOutlineThickness(1);
				rect.setFillColor(sf::Color(255,255,255));
				if (j == startTileX && i == startTileY)
				{
					rect.setFillColor(sf::Color(0,255,0));
				}

				if (j == finalTileX && i == finalTileY)
				{
					rect.setFillColor(sf::Color(255,0,0));
				}
				if (tiles.getTile(i,j) == via::TileType::OBSTACLE)
				{
					rect.setFillColor(sf::Color(0,0,0));
				}
				rect.setPosition(j*tiles.getTileSize()*scale,i*tiles.getTileSize()*scale);
				window.draw(rect);
			}
		}

		if (graphEnabled)
		{
			sf::CircleShape node(3);
			sf::Vertex line[2];

			node.setOutlineThickness(1);
			node.setOutlineColor(sf::Color(0,0,255));
			const std::vector<via::GraphNode*> nodes = g.getNodes();
			for (std::vector<via::GraphNode*>::const_iterator it = nodes.begin(); it != nodes.end(); it++)
			{
				node.setPosition(((*it)->getPosition().x * scale)-3, ((*it)->getPosition().y * scale)-3);
				window.draw(node);

				const std::vector<const via::Edge> connections = (*it)->getConnections();

				for (std::vector<const via::Edge>::const_iterator conIt = connections.begin(); conIt != connections.end(); conIt++)
				{
					line[0] = sf::Vertex(sf::Vector2f((*it)->getPosition().x * scale, (*it)->getPosition().y * scale),sf::Color(144,144,144,40));
					line[1] = sf::Vertex(sf::Vector2f(g.getNode((*conIt).to).getPosition().x * scale, g.getNode((*conIt).to).getPosition().y * scale),sf::Color(144,144,144,40));

					window.draw(line,2,sf::Lines);
				}

			
			}
		
		}

		if (node != 0 && goalNode != 0)
		{
			window.draw(line,2,sf::Lines);
			for (std::list<int>::iterator it = path.begin(); it != path.end(); it++)
			{

				std::list<int>::iterator it2 = std::next(it,1);

				if (it2 != path.end())
				{
					line[0] = sf::Vertex(sf::Vector2f(g.getNode(*it).getPosition().x * scale, g.getNode(*it).getPosition().y * scale),sf::Color(255,0,0));
					line[1] = sf::Vertex(sf::Vector2f(g.getNode(*(it2)).getPosition().x * scale, g.getNode(*(it2)).getPosition().y * scale),sf::Color(255,0,0));
					window.draw(line,2,sf::Lines);
				}
			}
			window.draw(line,2,sf::Lines);

		}
		//window.draw(line, 2, sf::Lines);
        window.display();
    }

    return 0;
}