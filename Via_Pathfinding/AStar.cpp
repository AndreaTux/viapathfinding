#include "AStar.h"
#include "IndexedPriorityQueueMin.h"
#include <iostream>

namespace via
{

AStar::AStar(const Graph& g)
	:graph(g),startingNodeIndex(-1),goalNodeIndex(-1),shortestPathTree(graph.size()),frontier(graph.size()),cumulativeCosts(graph.size(),0),expectedCosts(graph.size(),0){}

std::list<int> AStar::search(const SearchHeuristic& searchHeuristic, int startingNodeIndex, int goalNodeIndex)
{
	this->goalNodeIndex = goalNodeIndex;
	this->startingNodeIndex = startingNodeIndex;

	// start putting the initial node in the priority queue.
	IndexedPriorityQueueMin<float> nodesQueue(expectedCosts,graph.size());
	nodesQueue.insert(startingNodeIndex);

	// while there is at least one node to examine...
	while(!nodesQueue.empty())
	{
		//.. take the current node, basing on the lowest cost
		int currentNode = nodesQueue.pop();
		// move the node to the shortest path tree
		shortestPathTree[currentNode] = frontier[currentNode];

		// return the path if we found the goal node
		if (currentNode == goalNodeIndex)
		{
			return getPath();
		}


		// otherwise, get all the neighbours for the current node by getting its edges
		const std::vector<const Edge>& currentEdges = graph.getNode(currentNode).getConnections();

		for (std::vector<const Edge>::const_iterator it = currentEdges.begin(); it != currentEdges.end(); it++)
		{
			
			const Edge* currentEdge = &(*it);

			// compute the estimated cost from the current neighbour to the goal node
			float heuristicCost = 	searchHeuristic.calculate(graph, goalNodeIndex,currentEdge->to);
			// save the real cost of the node, given by the cost so far + the cost from the parent node to the current neighbour
			float realCost = cumulativeCosts[currentNode] + currentEdge->cost;

			// if it is still not in the frontier
			if (frontier[currentEdge->to] == NULL)
			{
				// update its costs
				expectedCosts[currentEdge->to] = heuristicCost + realCost;
				cumulativeCosts[currentEdge->to] = realCost;

				// insert the node into the queue and into the frontier
				nodesQueue.insert(currentEdge->to);
				frontier[currentEdge->to] = &(*currentEdge);

			}else // if the node was already been considered
			{
				// update its costs if we found a better path to reach it
				if ( realCost < cumulativeCosts[currentEdge->to] && shortestPathTree[currentEdge->to] == NULL)
				{
					expectedCosts[currentEdge->to] = realCost + heuristicCost;
					cumulativeCosts[currentEdge->to] = realCost;

					nodesQueue.updatePriority(currentEdge->to);
					frontier[currentEdge->to] = &(*currentEdge);
				}
			}
		}
	}
	return getPath();
}


std::vector<const Edge*> AStar::getShortestPathTree()const
{
	return shortestPathTree;
}

std::list<int> AStar::getPath()const
{
	std::list<int> path;

	//just return an empty path if no target or no path found
	if (goalNodeIndex < 0)  return path;    

	int node = goalNodeIndex;

	path.push_front(node);
    
	while ((node != startingNodeIndex) && (shortestPathTree[node] != 0))
	{
		node = shortestPathTree[node]->from;

		path.push_front(node);
	}

	return path;
}

float AStar::getPathCost()const
{
	return cumulativeCosts[goalNodeIndex];
}

}