#pragma once
#include "GraphNode.h"
#include "VMath.h"

namespace via
{

class SearchHeuristic
{
public:
	virtual float calculate(const Graph& graph, int start, int goal)const = 0;
};

class EuclideanDistanceHeuristic : public SearchHeuristic
{
public:
	float calculate (const Graph& graph, int start, int goal)const
	{
		return math::Vector2::distance(graph.getNode(start).getPosition(),graph.getNode(goal).getPosition());
	}
};

}