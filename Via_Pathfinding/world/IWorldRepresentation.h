#pragma once
#include "../GraphNode.h"

/*
* This interface must be implemented for each world representation. It defines the mandatory operation that a world representation
should offer.
*/

namespace via
{
class IWorldRepresentation
{
public:

	IWorldRepresentation()
		:graph(0){}

	// Builds a graph from the concrete representation
	virtual Graph& createGraph() = 0;
	// Proximity query: given a position, returns the closest node
	virtual GraphNode& getClosestNode(math::Vector2 pos) = 0;

	const Graph& getGraph()const
	{
		return *graph;
	}

protected:
	Graph* graph;
};

}