#include "TileWorldRepresentation.h"
#include <cassert>
#include <map>

namespace via
{

TileWorldRepresentation::TileWorldRepresentation(int w, int h, float tSize, bool neighbours, bool uniform)
	:width(w),height(h),tileSize(tSize),halfTileSize(tSize/2.f),eightNeighbours(neighbours),uniformCosts(uniform)
{
	levelData = new TileType*[height];

	for (int i = 0; i < height; i++)
	{
		levelData[i] = new TileType[width];
	}

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			levelData[i][j] = WALKABLE;
		}
	}

}

TileWorldRepresentation::TileWorldRepresentation(TileType** data, float tileSize, int w, int h, bool neighbours, bool uniform)
	:width(w),height(h),levelData(data),eightNeighbours(neighbours),uniformCosts(uniform){}

void TileWorldRepresentation::setTile(int x, int y, TileType content)
{
	assert (x >= 0 && x < height && y >= 0 && y < width);

	levelData[x][y] = content;
}

TileType TileWorldRepresentation::getTile(int x, int y)const
{
	assert (x >= 0 && x < height && y >= 0 && y < width);

	return levelData[x][y];
}

Graph& TileWorldRepresentation::createGraph()
{	
	graph = new Graph(false);

	// iterate over the grid and create nodes
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			// if the cell is walkable
			if (levelData[i][j] != OBSTACLE)
			{

				int cellNumber = getTileID(i,j);
				// assuming 0,0 is in top left corner
				math::Vector2 position (j * tileSize + halfTileSize, i * tileSize + halfTileSize);
				GraphNode* node = new GraphNode(position);
				graph->addNode(*node);
				tileNodeMapping[cellNumber] = node;
			}
		}
	}

	// iterate over the grid and create edges
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (levelData[i][j] != OBSTACLE)
			{
				int cellNumber = getTileID(i,j);
				// check for neighbours and add connections

				//check right
				if (j < width-1 && levelData[i][j+1] != OBSTACLE)
				{
					graph->addConnection(*tileNodeMapping[cellNumber],*tileNodeMapping[cellNumber+1]);
				}
				// check down
				if (i < height -1 && levelData[i+1][j] != OBSTACLE)
				{
					graph->addConnection(*tileNodeMapping[cellNumber],*tileNodeMapping[cellNumber+width]);
				}
				// if diagonal movement is allowed, check the other neighbours
				if (eightNeighbours)
				{
					//check right-down
					if (j < width-1 && i < height-1 && levelData[i+1][j+1] != OBSTACLE && (levelData[i+1][j] != OBSTACLE|| levelData[i][j+1] != OBSTACLE))
					{
						graph->addConnection(*tileNodeMapping[cellNumber],*tileNodeMapping[cellNumber+width+1]);
					}
					//check right - up
					if (j < width-1 && i > 0 && levelData[i-1][j+1] != OBSTACLE && (levelData[i-1][j] != OBSTACLE || levelData[i][j+1] != OBSTACLE))
					{
						graph->addConnection(*tileNodeMapping[cellNumber],*tileNodeMapping[cellNumber-width+1]);
					}
				}

			}
		}
	}

	return *graph;
}


GraphNode& TileWorldRepresentation::getClosestNode(math::Vector2 position)
{
	int tileX,tileY;
	getTileCoordinateFromPosition(position,tileX,tileY);
	return *tileNodeMapping.at(getTileID(tileX,tileY));
}

TileWorldRepresentation::~TileWorldRepresentation()
{
	for (int i = 0; i < height; i++)
	{
		delete [] levelData[i];
	}

	delete [] levelData;

	// delete the graph nodes -- is this a good design?
	const std::vector<GraphNode*>& nodes = graph->getNodes();

	for (std::vector<GraphNode*>::const_iterator it = nodes.begin(); it != nodes.end(); it++)
	{
		delete (*it);
	}
}

int TileWorldRepresentation::getHeight()const
{
	return height;
}

int TileWorldRepresentation::getWidth()const
{
	return width;
}

float TileWorldRepresentation::getTileSize()const
{
	return tileSize;
}

GraphNode& TileWorldRepresentation::getNodeFromTile(int tile)const
{
	assert (tileNodeMapping.find(tile) != tileNodeMapping.end());

	return *(tileNodeMapping.at(tile));
}

GraphNode& TileWorldRepresentation::getNodeFromTile(int row, int col)const
{
	assert (row < width && col < height);

	int tileNum = row * width + col;

	return getNodeFromTile(tileNum);
}


void TileWorldRepresentation::getTileCoordinateFromPosition(math::Vector2 position, int& row, int& col)
{
	row = position.x / tileSize;
	col = position.y / tileSize;
}

int TileWorldRepresentation::getTileID(int x, int y)const
{
	return width * x + y;
}

bool TileWorldRepresentation::isWalkable(int x, int y)
{
	if (x < 0 || y < 0 || x >= height || y >= width)
	{
		return false;
	}

	return levelData[x][y] != OBSTACLE;
}

}
