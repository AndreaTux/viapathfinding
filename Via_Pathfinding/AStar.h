#pragma once
#include "GraphNode.h"
#include "SearchHeuristic.h"
#include "ISearch.h"
#include <list>

/* 
* Implements general A* graph search
*/

namespace via
{

class AStar : public ISearch
{
public:
	AStar(const Graph& graph);
	virtual std::list<int> search(const SearchHeuristic& heuristic, int startingNodeIndex, int goalNodeIndex);

	std::vector<const Edge*> getShortestPathTree()const;
	
	float getPathCost()const;

protected:
	const Graph& graph;
	std::vector<float> cumulativeCosts; // Represents the "G" costs
	std::vector<float> expectedCosts; // Represents the F = G + H costs

	std::vector<const Edge*> shortestPathTree;
	std::vector<const Edge*> frontier;

	std::list<int> getPath()const;

	int startingNodeIndex;
	int goalNodeIndex;

};

}

