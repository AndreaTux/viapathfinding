#pragma once
#include "ISearch.h"
#include "world/TileWorldRepresentation.h"
#include "SearchHeuristic.h"

namespace via
{
#define DEFAULT_TILE    0x01
#define DIAGONAL_TILE  0x02
class ViaPathfinding
{
public:
	ViaPathfinding();
	~ViaPathfinding();

	TileWorldRepresentation& createTileWorldRepresentation(int rows, int cols, float tileSize, bool diagonalAllowed, bool uniformCosts);
	const Graph& createGraphFromCurrentRepresentation()const;
	const Graph& getGraph()const;

	std::list<int> findPath(math::Vector2& startingPos, math::Vector2& goalPos);
	std::list<int> findPath(const GraphNode& startNode, const GraphNode& goalNode);

private:

	ISearch* searchAlgorithm;
	IWorldRepresentation* worldRepresentation;
	SearchHeuristic* heuristic;
	unsigned char status;
	//Graph* graph;

	void deleteMembers();
	void setStatus(unsigned char flag);
	unsigned char getStatus(unsigned char flag);
};
}
