#pragma once
#include <list>
#include "SearchHeuristic.h"

/*
* Defines an interface that every search algorithm must adhere.
*/

namespace via
{
class ISearch
{
public:
	// Uses the heursitic h to find a list of node index from "startNode" to "goalNdoe".
	virtual std::list<int> search(const SearchHeuristic& h,int startNode, int goalNode) = 0;

};
}