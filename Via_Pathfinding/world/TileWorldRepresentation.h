#pragma once
#include "IWorldRepresentation.h"
#include "../VMath.h"
/*
* This class represents a tile based world. A tiled based world representation is composed of tiles of the same size,
supports diagonal walking and can be fully customized by the user.
*/

namespace via
{
enum TileType
{
	WALKABLE = 1,
	OBSTACLE = -1
};

class TileWorldRepresentation : public IWorldRepresentation
{
public:
	// Create a tile grid filled with WALKABLE positions given the specified parameters. 
	TileWorldRepresentation(int width, int height, float tileSize, bool eightNeighbour, bool uniformCosts);
	// Create a tile grid using the user defined level data and the specified parameters.
	TileWorldRepresentation(TileType** levelData, float tileSize, int width, int height,bool eightNeighbour, bool uniformCosts); // not yet considered/tested
	// Set a tile content.
	void setTile(int x, int j, TileType content);
	// Returns the tile number, given a tile row and column.
	int getTileID(int x, int y)const;
	bool isWalkable(int x, int y);
	// Performs proximity query.
	GraphNode& getClosestNode(math::Vector2 position);
	// Given a position, it returns the row and the column of the tile which contain that position.
	void getTileCoordinateFromPosition(math::Vector2 position, int& row, int& col);
	GraphNode& getNodeFromTile(int tile)const;
	GraphNode& getNodeFromTile(int x, int y)const;
	// Given a node index, it returns the coordinate of the relative tile.
	void getTilePositionFromNode(int nodeIndex, int& tileRow, int& tileCol);
	TileType getTile(int x, int j)const;
	int getWidth()const;
	int getHeight()const;
	float getTileSize()const;
	~TileWorldRepresentation();

	// Generates the graph for the given grid. TODO: optimize for uniform grids as is possible to generate fewer nodes with JPS
	virtual Graph& createGraph();

private:
	// The representation of the grid as a matrix of TileType type.
	TileType** levelData;
	// A handy mapping between tiles ID and graph nodes.
	std::map<int,GraphNode*> tileNodeMapping;
	int width;
	int height;
	float tileSize;
	float halfTileSize;
	bool eightNeighbours;
	bool uniformCosts;
};
}

