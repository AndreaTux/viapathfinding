#pragma once
#include <math.h>

/*
* Math class aided to provide data structures and common math functions
*/

namespace via
{
namespace math
{

/*
* Implements a vector of two elements with single pricision.
*/

struct Vector2
{

	Vector2():x(0),y(0){}
	
	Vector2 (float x, float y)
	{
		this->x = x;
		this->y = y;
	}

	static float distance(const Vector2& a, const Vector2& b)
	{
		float dx = b.x - a.x;
		float dy = b.y - a.y;

		return sqrt(dx * dx + dy * dy);
	}

	float x;
	float y;
};

// clamps the number n between lower and upper
static float clamp(float n, float lower, float upper)
{
		return n <= lower ? lower : n >= upper ? upper : n;
}

} // math namespace

} // via namespace

