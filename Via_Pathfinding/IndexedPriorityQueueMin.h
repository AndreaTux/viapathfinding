#pragma once
#include <vector>
#include <cassert>

namespace via
{
template <class Element>
class IndexedPriorityQueueMin
{
public:

	IndexedPriorityQueueMin(std::vector<Element>& keys, int mSize)
		:elements(keys), maxSize(mSize), size(0)
	{
		heap.assign(mSize+1,0);
		inverseHeap.assign(mSize+1,0);
	}

	int pop()
	{
		swap(1,size);
		reorderDownwards(1,size-1);
		return heap[size--];
	}

	void updatePriority(int indx)
	{
		reorderUpwards(inverseHeap[indx]);
	}

	bool empty()const
	{
		return size == 0;
	}

	void insert (int indx)
	{
		assert (size + 1 <= maxSize );

		size++;
		heap[size] = indx;
		inverseHeap[indx] = size;
		reorderUpwards(size);
	}
private:

	void swap (int a, int b)
	{
		int tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;

		inverseHeap[heap[a]] = a;
		inverseHeap[heap[b]] = b;
	}

	void reorderUpwards (int d)
	{
		while (d > 1 && elements[heap[d/2]] > elements[heap[d]])
		{
			swap(d/2,d);
			d = d/2;
		}
	}

	void reorderDownwards (int d, int size)
	{
		while ( 2 * d <= size)
		{
			int child = 2 * d;

			if ( child < size && elements[heap[child]] > elements[heap[child+1]])
			{
				child++;
			}

			if (elements[heap[d]] > elements[heap[child]])
			{
				swap(child,d);
				d = child;
			}
			else
			{
				break;
			}
		}
	}

private:
	std::vector<Element>& elements;
	std::vector<int> heap;
	std::vector<int> inverseHeap;

	int size;
	int maxSize;
};

}