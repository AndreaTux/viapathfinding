#pragma once
#include "AStar.h"
#include "world\TileWorldRepresentation.h"

namespace via
{
class JumpPointSearch : public AStar
{
public:
	JumpPointSearch(TileWorldRepresentation& grid);
	~JumpPointSearch(void);

	virtual std::list<int> search(const SearchHeuristic& heuristic, int startingNodeIndex, int goalNodeIndex);

private:
	const GraphNode* jumpToNode(int currentTileX, int currentTileY, int dx, int dy, int startNodeIndex, int goalNodeIndex);
	TileWorldRepresentation& grid;
};
}

