#include "ViaPathfinding.h"
#include "AStar.h"
#include <cassert>

namespace via
{
ViaPathfinding::ViaPathfinding()
	:worldRepresentation(0),searchAlgorithm(0){
}


TileWorldRepresentation& ViaPathfinding::createTileWorldRepresentation(int row, int col, float tileSize, bool diagonalAllowed, bool uniformCosts)
{
	deleteMembers();
	worldRepresentation = new TileWorldRepresentation(row,col,tileSize,diagonalAllowed,uniformCosts);

	if (diagonalAllowed)
	{
		setStatus(DIAGONAL_TILE);
	}else
	{
		setStatus(DEFAULT_TILE);
	}
	return ((TileWorldRepresentation&)*worldRepresentation);
}

ViaPathfinding::~ViaPathfinding()
{
	deleteMembers();
}

void ViaPathfinding::deleteMembers()
{
	if (worldRepresentation != 0)
	{
		delete worldRepresentation;
	}

	if (searchAlgorithm != 0)
	{
		delete searchAlgorithm;
	}
}

const Graph& ViaPathfinding::getGraph()const
{
	return worldRepresentation->getGraph();
}

std::list<int> ViaPathfinding::findPath(math::Vector2& startingPos, math::Vector2& goalPos)
{
	assert (worldRepresentation != 0);
	const Graph& g = worldRepresentation->getGraph();
	return findPath(worldRepresentation->getClosestNode(startingPos),worldRepresentation->getClosestNode(goalPos));
}

std::list<int> ViaPathfinding::findPath(const GraphNode& startNode, const GraphNode& goalNode)
{
	const Graph& g = worldRepresentation->getGraph();
	if (searchAlgorithm != 0)
	{
		delete searchAlgorithm;
	}
	if(getStatus(DEFAULT_TILE))
	{
		searchAlgorithm = new AStar(g);
	}else
	if (getStatus(DIAGONAL_TILE))
	{
		// TODO put JPS
		searchAlgorithm = new AStar(g);
	}
	heuristic = new EuclideanDistanceHeuristic;

	return searchAlgorithm->search(*heuristic,startNode.getIndex(),goalNode.getIndex());
}

void ViaPathfinding::setStatus(unsigned char flag)
{
	status |= flag;
}

unsigned char ViaPathfinding::getStatus(unsigned char flag)
{
    return (status & flag) != 0;
}

const Graph& ViaPathfinding::createGraphFromCurrentRepresentation()const
{
	return worldRepresentation->createGraph();
}

}
