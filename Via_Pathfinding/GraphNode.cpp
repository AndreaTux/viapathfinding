#include "GraphNode.h"
#include <cassert>

namespace via
{

GraphNode::GraphNode()
	:index(DETACHED),userData(0){}

GraphNode::GraphNode(math::Vector2 pos)
	:index(DETACHED),userData(0),position(pos){}

GraphNode::~GraphNode(){}

int GraphNode::getIndex()const
{
	return index;
}

void GraphNode::addConnection(const Edge edge)
{
	// only add if it is unique
	bool unique = true;
	for (std::vector<const Edge>::iterator it = connections.begin(); it != connections.end(); it++)
	{
		if ((*it).to == edge.to)
		{
			unique = false;
			break;
		}
	}

	if (unique)
	{
		connections.push_back(edge);
	}
}

const math::Vector2& GraphNode::getPosition()const
{
	return position;
}

void GraphNode::addConnection(GraphNode other, float cost )
{
	assert (this->index != other.index);

	addConnection(Edge(this->index,other.index,cost));
}


// Graph class
Graph::Graph(bool dGraph)
	:nodesCount(0),directedGraph(dGraph){}

GraphNode& Graph::getNode(int index)
{
	assert(index < (int)nodes.size() && index >= 0);

	return *nodes[index];
}

const std::vector<const Edge>& GraphNode::getConnections()const
{
	return connections;
}

const GraphNode& Graph::getNode(int index)const
{
	assert(index < (int)nodes.size() && index >= 0 && index != INVALID);

	return *nodes[index];
}

const std::vector<GraphNode*>& Graph::getNodes()const
{
	return nodes;
}

int Graph::addNode(GraphNode& node)
{
	
	assert (node.index == DETACHED);
	node.index = nodesCount;
	nodesCount++;
	nodes.push_back(&node);
	 
	return node.index;
}

// TODO: node still remains in memory
void Graph::removeNode(int index)
{
	getNode(index).index = INVALID;
}

int Graph::size()const
{
	return nodesCount;
}

void Graph::addConnection(int from, int to, float cost)
{
	assert (from < nodesCount && to < nodesCount);
	
	if (nodes[from]->index != INVALID && nodes[to]->index != INVALID)
	{
		Edge e (from,to,cost);
		nodes[from]->addConnection(e);
	}
}

void Graph::addConnection(GraphNode& from, GraphNode& to, float cost)
{
	assert ( from.index < nodesCount && to.index < nodesCount );

	if (nodes[from.index]->index != INVALID && nodes[to.index]->index != INVALID)
	{
		nodes[from.index]->addConnection(to,cost);

		if (!directedGraph)
		{
			nodes[to.index]->addConnection(from,cost);
		}
	}
}

void Graph::removeConnection (int from, int to)
{
	assert (from < nodesCount && to < nodesCount);

	if (nodes[from]->index != INVALID && nodes[to]->index != INVALID)
	{
		for (std::vector<const Edge>::iterator it = nodes[from]->connections.begin(); it != nodes[from]->connections.end();)
		{
			if ((*it).to == to)
			{
				it = nodes[from]->connections.erase(it);
				break;
			}else
			{
				it++;
			}
		}
	}
}



} //namespace