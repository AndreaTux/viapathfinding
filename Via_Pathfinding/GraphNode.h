#pragma once
#include <map>
#include <vector>
#include "vmath.h"


namespace via{

enum NodeState
{
	DETACHED = -1,
	INVALID = -2

};

/*
* Represents a node weighted edge.
*/
struct Edge
{
	Edge(int start, int end)
		:from(start),to(end),cost(1.f){}

	Edge(int start, int end, float c)
		:from(start),to(end),cost(c){}
	
	float cost;
	int from;
	int to;
};

// forward declaration
class Graph;

/*
* Class for a graph node. Each node has an unique index, can store user data and holds all the connections starting from it.
*/

class GraphNode
{
	friend class Graph;
public:
	GraphNode();
	GraphNode(math::Vector2 pos);
	virtual ~GraphNode();

	const std::vector<const Edge>& getConnections()const;
	const math::Vector2& getPosition()const;
	void addConnection(const GraphNode other, float cost = 0);
	void addConnection(const Edge e);
	void removeConnection(Edge e);

	int getIndex()const;

protected:
	int index;
	std::vector<const Edge> connections;
	void* userData; // can be used in a search -- not supported yet
	math::Vector2 position;
};

/*
* The graph class represents a collection of nodes. A graph can be directed or not. To save
* time, a node which is removed is set to be INVALID, but it will still be present in the graph.
*/

class Graph
{
public:
	Graph(bool directedGraph);

	GraphNode& getNode(int index);
	const GraphNode& getNode(int index)const;
	const std::vector<GraphNode*>& getNodes()const;

	int addNode(GraphNode& node);
	void addConnection(int from, int to, float cost = 0);
	void addConnection(GraphNode& from, GraphNode& to, float cost = 0);
	void removeConnection (int from, int to);
	void removeNode(int index);

	int size()const;

private:
	//TODO: this forces the ID to be inside the vector size; use hash instead.
	std::vector<GraphNode*> nodes;
	int nodesCount;
	bool directedGraph;

};

}
